const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

// Instantiate Express
const app = express();

// BodyParse Middleware initialization
app.use(bodyParser.json());

app.get("/users", (req, res) => {
  const customers = [
    { id: 1, firstName: "John", lastName: "Doe" },
    { id: 2, firstName: "Steve", lastName: "Smith" },
    { id: 3, firstName: "Naeem", lastName: "Arshad" },
    { id: 4, firstName: "Mary", lastName: "Swanson" }
  ];
  res.json(customers);
});

// Server Port
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server Started on port: ${port}`));